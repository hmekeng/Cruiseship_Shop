<?php

/**
 * @file
 * Admin setting Cruiseship
 */
/**
 * function for the admin path customer_data_collection_admin().
 */
function customer_data_collection_admin()
{
    $form['header_text'] = array(
        '#type' => 'textarea',

        '#title' => t('Introductory text'),

        '#default_value' => variable_get('introductory_text',
          t("Participate in the contest and win a luxurious 1-week cruise on the river Nile!, where?
           luxurious 1-week cruise on the river Nile is emphasized")),

        '#description' => t("Panel to edit an introductory text."),
        '#required' => TRUE,
    );
    return system_settings_form($form);
}

/**
 * Implementation of customer_data_collection_admin().
 *
 * @variable_set set the introduction text
 */
function customer_data_collection_admin_validate($form, &$form_state)
{
    $header_text = $form_state['values']['header_text'];
    variable_set('introductory_text', $header_text);
}
